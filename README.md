# Antora antora-shiki Pipeline Extension
:version: 0.0.1

`@djencks/antora-shiki` provides an Asciidoctor.js build-time SyntaxHighlighter using shiki installed via an Antora pipeline extension.

NOTE: for more complete, better formatted README, see https://gitlab.com/djencks/antora-shiki/-/blob/master/README.adoc.

## Installation

Available soon through npm as @djencks/antora-shiki.

Currently available through a line like this in `package.json`:


    "@djencks/antora-shiki": "https://gitlab.com/djencks/antora-shiki",


The project git repository is https://gitlab.com/djencks/antora-shiki

## Usage in asciidoctor.js

This requires investigation and custom registration coding.
See https://gitlab.com/djencks/antora-shiki/-/blob/master/README.adoc

## Usage in Antora
See https://gitlab.com/djencks/antora-shiki/-/blob/master/README.adoc

## Antora Example project

An example project showing simple use of this extension is under extensions/shiki-highlighter in `https://gitlab.com/djencks/simple-examples`.
