'use strict'
/* global Opal */

const shiki = require('shiki')
const DEFAULT_THEME = 'github-light'

let highlighter

const START_RX = /<pre class="shiki" style="background-color: #[0-9a-f]*"><code>/
const END = '</code></pre>'

class ShikiSyntaxHighlighter {
  format (node, lang, opts = {}) {
    opts = Object.assign({}, opts, {
      transform: (x, code) => {
        code['$[]=']('class', `language-${lang || 'none'} shiki`)
      },
    })

    const toHash = function (object) {
      return object && !object.$$is_hash ? Opal.hash2(Object.keys(object), object) : object
    }
    return Opal.send(this, Opal.find_super_dispatcher(this, 'format', this.$format), [node, lang, toHash(opts)])
  }

  handlesHighlighting () { return true }

  highlight (node, source, lang, opts) {
    try {
      let html = highlighter.codeToHtml(source, lang)
      //Leaving the html default results in nested pre/code elements,
      // which is rendered as an unattractive box around the highlighted code.
      html = html.replace(START_RX, '')
      html = html.slice(0, -END.length)
      return html
    } catch (e) {
      //TODO implement logging
      console.log(e.message)
      return source
    }
  }
}

module.exports.register = function (registry, config) {
  config.length || (config.theme = DEFAULT_THEME)
  registry.on('contentClassified', async ({ siteAsciiDocConfig }) => {
    highlighter = await shiki.getHighlighter(config)
    const register = { register: asciidoctorRegister }
    siteAsciiDocConfig.extensions
      ? siteAsciiDocConfig.extensions.push(register) : siteAsciiDocConfig.extensions = [register]
  })
}

function doRegister (registry) {
  const AsciidoctorModule = registry.$$base_module
  const SyntaxHighlighterRegistry = AsciidoctorModule.$$.SyntaxHighlighter
  SyntaxHighlighterRegistry.register('shiki', ShikiSyntaxHighlighter)
}

function asciidoctorRegister (registry, config) {
  if (typeof registry.register === 'function') {
    doRegister(registry)
  } else {
    const clazz = registry.$$class
    const extensionsModule = clazz.$$base_module
    doRegister(extensionsModule)
  }
  return registry
}
