/* eslint-env mocha */
'use strict'

//Opal is apt to be needed for Antora tests, perhaps not otherwise
// IMPORTANT eagerly load Opal since we'll always be in this context; change String encoding from UTF-16LE to UTF-8
const { Opal } = require('asciidoctor-opal-runtime')
if ('encoding' in String.prototype && String(String.prototype.encoding) !== 'UTF-8') {
  String.prototype.encoding = Opal.const_get_local(Opal.const_get_qualified('::', 'Encoding'), 'UTF_8') // eslint-disable-line
}
const asciidoctor = require('@asciidoctor/core')()
const highlighter = require('./../lib/antora-shiki')
const { expect } = require('chai')

describe('antora-shiki tests', () => {
  async function load (text, registry) {
    let listener
    const pipeline = { on: (event, f) => { listener = f } }
    highlighter.register(pipeline, {})
    const siteAsciiDocConfig = {}
    await listener({ siteAsciiDocConfig })
    expect(siteAsciiDocConfig.extensions.length).to.equal(1)
    siteAsciiDocConfig.extensions[0].register(registry)
    const options = { attributes: { 'source-highlighter': 'shiki' } }
    return asciidoctor.load(text, options)
  }

  ;[
    [asciidoctor.Extensions, 'global'],
    [asciidoctor.Extensions.create(), 'registry'],
  ].forEach(([registry, name]) => {
    it(`highlighting happens (${name})`, async () => {
      const doc = await load(`
[source,js]
----
class HighlightSyntaxHighlighter extends SyntaxHighlighterBase {
  handlesHighlighting () { return true }

  highlight (node, source, lang, opts) {
    return hljs.highlight(lang, source).value
  }
}
----
`, registry)
      const html = doc.convert()
      expect(html).to.contain('<code class="language-js shiki" data-lang="js">')
      expect(html).to.contain('<span style="color: #D73A49">class</span>')
    })

    it(`conums (${name})`, async () => {
      const doc = await load(`
[source,js]
----
class HighlightSyntaxHighlighter extends SyntaxHighlighterBase {
  handlesHighlighting () { return true } //<1>

  highlight (node, source, lang, opts) {
    return hljs.highlight(lang, source).value
  }
}
----
`, registry)
      const html = doc.convert()
      expect(html).to.contain('//<b class="conum">(1)</b>')
    })
  })
})
